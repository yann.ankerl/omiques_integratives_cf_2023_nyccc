#!/usr/bin/bash

bedtools window -a genomic_genes.gff -b hyperDMR_18h_merged.bedGraph > genes18hype.gff
bedtools window -a genomic_genes.gff -b hypoDMR_18h_merged.bedGraph > genes18hypo.gff

bedtools window -a genomic_genes.gff -b hyperDMR_0h_merged.bedGraph > genes0hype.gff
bedtools window -a genomic_genes.gff -b hypoDMR_0h_merged.bedGraph > genes0hypo.gff

bedtools window -a genomic_genes.gff -b hyperDMR_control_merged.bedGraph > geneschype.gff
bedtools window -a genomic_genes.gff -b hypoDMR_control_merged.bedGraph > geneschypo.gff

awk -F "[=;]" 'BEGIN {OFS="\t"} {print $2}' genes0hype.gff | awk -F "-" '{print $2}' > list_genes0hype.txt
awk -F "[=;]" 'BEGIN {OFS="\t"} {print $2}' genes18hype.gff | awk -F "-" '{print $2}' > list_genes18hype.txt
awk -F "[=;]" 'BEGIN {OFS="\t"} {print $2}' geneschype.gff | awk -F "-" '{print $2}' > list_geneschype.txt

awk -F "[=;]" 'BEGIN {OFS="\t"} {print $2}' genes0hypo.gff | awk -F "-" '{print $2}' > list_genes0hypo.txt
awk -F "[=;]" 'BEGIN {OFS="\t"} {print $2}' genes18hypo.gff | awk -F "-" '{print $2}' > list_genes18hypo.txt
awk -F "[=;]" 'BEGIN {OFS="\t"} {print $2}' geneschypo.gff | awk -F "-" '{print $2}' > list_geneschypo.txt

grep -vf <(cat list_geneschype.txt) <(grep -vf <(cat list_genes0hype.txt) <(cat list_genes18hype.txt)) > DMR_hyper_genes.txt
grep -vf <(cat list_geneschypo.txt) <(grep -vf <(cat list_genes0hypo.txt) <(cat list_genes18hypo.txt)) > DMR_hypo_genes.txt
cat counts_0h_10cov.txt counts_18h_10cov.txt counts_control_10cov.txt | awk -F "[-\t]" '{print $2}' | sort -u > rrbs_mapped_genes.txt
