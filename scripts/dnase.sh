#!/usr/bin/bash

# List genes from DNAse data

# DHS
awk '{print $4}' ikaros0_consensus_dhs_refseq.bed | sort | uniq > ikaros0_dhs.id
awk '{print $4}' ikaros18_consensus_dhs_refseq.bed | sort | uniq > ikaros18_dhs.id
awk '{print $4}' control18_consensus_dhs_refseq.bed | sort | uniq > control18_dhs.id
awk '{print $4}' all_consensus_dhs_refseq.bed | sort | uniq > all_dhs.id
awk '{print $4}' control0_consensus_dhs_refseq.bed | sort | uniq > control0_dhs.id

# Comparisons
grep -vwf ikaros0_dhs.id ikaros18_dhs.id > ik18-ik0.id
grep -vwf control0_dhs.id control18_dhs.id > c18-c0.id
grep -vwf control0_dhs.id ikaros18_dhs.id > ik0-c0.id
grep -vwf c18-c0.id ik18-ik0.id > ik18_0-c18_0.id
grep -vwf ik0-c0.id ik18_0-c18_0.id > ikaros_dhs.id

# DHS positions
grep -wf ikaros_dhs.id ../ikaros18_consensus_dhs_refseq.bed > ikaros_dhs.bed

# DHS positions / genes
bedtools window -a ../../../../genomic_genes.gff -b ikaros_dhs.bed > DNAse_ikaros_W.gff

# Genes ID extraction
awk -F "[=;]" 'BEGIN {OFS="\t"} {print $2}' DNAse_ikarosuniqW.gff | cut -c 6- | sort | uniq > list_genesDNAse_open.txt

# Genes influenced by Ikaros ("peak loss")
grep -vwf ikaros18_dhs.id ikaros0_dhs.id > ik0-ik18.id
grep -vwf control18_dhs.id ik0-ik18_dhs.id > ikaros_pl.id

# DHS positions
grep -wf ikaros_pl.id ../ikaros0_consensus_dhs_refseq.bed > ikaros_pl.bed

# DHS positions / genes
bedtools window -a ../../../../genomic_genes.gff -b ikaros_pl.bed > DNAse_ikaros_L.gff
