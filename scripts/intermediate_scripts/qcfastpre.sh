#!/usr/bin/bash

accessions=(SRR2960405 SRR2960406 SRR2960407 SRR2960417 SRR2960418 SRR2960419 SRR2960423 SRR2960424 SRR2960425 SRR2960435 SRR2960436 SRR2960437)

for acc in "${accessions[@]}"; do
        nohup fastqc "$acc"_1.fastq -o /home/projet/QualityControls/fastqcPreTrimming &
	nohup fastqc "$acc"_2.fastq -o /home/projet/QualityControls/fastqcPreTrimming &
done
