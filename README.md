# Omiques_intégratives_CF_2023_NYCCC

## Membres

Groupe 3 : Yassine ANKERL, Corentin BONHOMME, Cecilia DEFONTIS, Naell LENOIR, Quentin NUGIER.  

## Contexte

Ce dépôt git concentre l'ensemble des scripts produits lors de l'UE Omiques Intégratives dans le but de traiter un sous-set du jeu de données de l'étude **STATegra** (Gomez-Cabrero et al. (2019) : STATegra, a comprehensive multi-omics dataset of B-cell differentiation in mouse, Scient. Data 6:256 (doi.org/10.1038/s41597- 019-0202-7)).  


## Question biologique



## Requirements

L'environnement utilisé lors du projet est accessible par le fichier **omique.yml**.  

## Scripts

Le script **pipeline_rrbs** permet le pré-traitement, l'alignement et l'analyse de la méthylation des données RRBS.  
Chaque étape de ce pipeline peut se retrouver dans un script lui correspondant dans le dossier **intermediate_scripts**.  
Les scripts ayant permis l'extraction des listes de gènes provenant de l'analyse des données RNA-seq et DNAse-seq sont également présents dans le dossier **scripts**, au même niveau que pipeline_rrbs.  


## Organisation et répartition des travaux

Corentin : exploration de la documentation, test des fonctions et des outils pour trouver les commandes adéquates, préparation des données d'intégration DNAse-seq et RNA-seq, analyse et visualisation des résultats de methylkit (IGV, UCSC), traitement des fichiers.  
Quentin : exploration de la documentation pour trouver les commandes adéquates, test des scripts, préparation des données d'intégration RNA-seq et DNAse-seq, analyse des données produites par methylKit (IGV, UCSC), traitement des fichiers.  
Naell : téléchargement et partage des données, automatisation des scripts pour le traitement des données à partir des informations recoltées par Corentin et Quentin, préparation des données d'intégration RNA-seq, mise en place du pipeline, correction du diaporama.  
Cecilia : réalisation de graphiques à partir des données obtenues pour chaque omique, visualisation (IGV, UCSC), intégration des données DNAse-seq, réalisation du diaporama.  
Yassine : aide à la réflexion, regroupement des listes de gènes des différentes omiques en un tableau pour finaliser l'intégration, partie sommaire du diaporama.  
