#!/usr/bin/bash

accessions=(SRR2960405 SRR2960406 SRR2960407 SRR2960417 SRR2960418 SRR2960419 SRR2960423 SRR2960424 SRR2960425 SRR2960435 SRR2960436 SRR2960437)

for acc in "${accessions[@]}"; do
	nohup bismark /home/projet/GenomeRef_mm39/ncbi_dataset/data/GCF_000001635.27/ -1 Trimming/"$acc"_1_val_1.fq -2 Trimming/"$acc"_2_val_2.fq -o BismarkResults/ &
done
