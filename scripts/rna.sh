#!/usr/bin/bash

# List over and under expressed genes

# Overexpressed genes
awk 'BEGIN {OFS="\t"} $3>1 {print $1}' control0_vs_control18_signi.tsv > control0_vs_control18_up.tsv
awk 'BEGIN {OFS="\t"} $3>1 {print $1}' control0_vs_ikaros0_signi.tsv > control0_vs_ikaros0_up.tsv
awk 'BEGIN {OFS="\t"} $3>1 {print $1}' control18_vs_ikaros18_signi.tsv > control18_vs_ikaros18_up.tsv
grep -wvf <(cat control0_vs_control18_up.tsv) <(grep -wvf <(cat control0_vs_ikaros0_up.tsv) <(cat control18_vs_ikaros18_up.tsv)) > RNA_up_genes.txt

# Underexpressed genes
awk 'BEGIN {OFS="\t"} $3>1 {print $1}' control0_vs_control18_signi.tsv > control0_vs_control18_up.tsv
awk 'BEGIN {OFS="\t"} $3>1 {print $1}' control0_vs_ikaros0_signi.tsv > control0_vs_ikaros0_up.tsv
awk 'BEGIN {OFS="\t"} $3>1 {print $1}' control18_vs_ikaros18_signi.tsv > control18_vs_ikaros18_up.tsv
grep -wvf <(cat control0_vs_control18_down.tsv) <(grep -wvf <(cat control0_vs_ikaros0_down.tsv) <(cat control18_vs_ikaros18_down.tsv)) > RNA_down_genes.txt
