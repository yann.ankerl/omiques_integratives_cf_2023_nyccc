#!/usr/bin/bash
OUTDIR_SORT=/home/naell/methkit/BismarkResults/sorted
control_0h=($(ls $OUTDIR_SORT/SRR296040* | xargs -n 1 basename))
control_18h=($(ls $OUTDIR_SORT/SRR296041* | xargs -n 1 basename))
ikaros_0h=($(ls $OUTDIR_SORT/SRR296042* | xargs -n 1 basename))
ikaros_18h=($(ls $OUTDIR_SORT/SRR296043* | xargs -n 1 basename))

cd $OUTDIR_SORT

echo "Running methylkit : control..."
nohup Rscript /home/projet/scripts/mini_scripts/Mkit_control_100.R ${control_0h[0]} ${control_0h[1]} ${control_0h[2]} ${control_18h[0]} ${control_18h[1]} ${control_18h[2]} &
nohup Rscript /home/projet/scripts/mini_scripts/Mkit_control_500.R ${control_0h[0]} ${control_0h[1]} ${control_0h[2]} ${control_18h[0]} ${control_18h[1]} ${control_18h[2]} &
nohup Rscript /home/projet/scripts/mini_scripts/Mkit_control_1000.R ${control_0h[0]} ${control_0h[1]} ${control_0h[2]} ${control_18h[0]} ${control_18h[1]} ${control_18h[2]} &

echo "Running methylkit : ikaros..."
nohup Rscript /home/projet/scripts/mini_scripts/Mkit_ikaros_100.R ${ikaros_0h[0]} ${ikaros_0h[1]} ${ikaros_0h[2]} ${ikaros_18h[0]} ${ikaros_18h[1]} ${ikaros_18h[2]} &
nohup Rscript /home/projet/scripts/mini_scripts/Mkit_ikaros_500.R ${ikaros_0h[0]} ${ikaros_0h[1]} ${ikaros_0h[2]} ${ikaros_18h[0]} ${ikaros_18h[1]} ${ikaros_18h[2]} &
nohup Rscript /home/projet/scripts/mini_scripts/Mkit_ikaros_1000.R ${ikaros_0h[0]} ${ikaros_0h[1]} ${ikaros_0h[2]} ${ikaros_18h[0]} ${ikaros_18h[1]} ${ikaros_18h[2]} &

echo "Running methylkit : T0..."
nohup Rscript /home/projet/scripts/mini_scripts/Mkit_0h_100.R ${control_0h[0]} ${control_0h[1]} ${control_0h[2]} ${ikaros_0h[0]} ${ikaros_0h[1]} ${ikaros_0h[2]} &
nohup Rscript /home/projet/scripts/mini_scripts/Mkit_0h_500.R ${control_0h[0]} ${control_0h[1]} ${control_0h[2]} ${ikaros_0h[0]} ${ikaros_0h[1]} ${ikaros_0h[2]} &
nohup Rscript /home/projet/scripts/mini_scripts/Mkit_0h_1000.R ${control_0h[0]} ${control_0h[1]} ${control_0h[2]} ${ikaros_0h[0]} ${ikaros_0h[1]} ${ikaros_0h[2]} &

echo "Running methylkit : T18..."
nohup Rscript /home/projet/scripts/mini_scripts/Mkit_18h_100.R ${control_18h[0]} ${control_18h[1]} ${control_18h[2]} ${ikaros_18h[0]} ${ikaros_18h[1]} ${ikaros_18h[2]} &
nohup Rscript /home/projet/scripts/mini_scripts/Mkit_18h_500.R ${control_18h[0]} ${control_18h[1]} ${control_18h[2]} ${ikaros_18h[0]} ${ikaros_18h[1]} ${ikaros_18h[2]} &
nohup Rscript /home/projet/scripts/mini_scripts/Mkit_18h_1000.R ${control_18h[0]} ${control_18h[1]} ${control_18h[2]} ${ikaros_18h[0]} ${ikaros_18h[1]} ${ikaros_18h[2]} &

wait

mv *control_100.bedGraph methylKit_control_100/
mv *control_500.bedGraph methylKit_control_500/
mv *control_1000.bedGraph methylKit_control_1000/

mv *ikaros_100.bedGraph methylKit_ikaros_100/
mv *ikaros_500.bedGraph methylKit_ikaros_500/
mv *ikaros_1000.bedGraph methylKit_ikaros_1000/

mv *0h_100.bedGraph methylKit_0h_100/
mv *0h_500.bedGraph methylKit_0h_500/
mv *0h_1000.bedGraph methylKit_0h_1000/

mv *18h_100.bedGraph methylKit_18h_100/
mv *18h_500.bedGraph methylKit_18h_500/
mv *18h_1000.bedGraph methylKit_18h_1000/

echo "Methylkit analysis done for all conditions."
