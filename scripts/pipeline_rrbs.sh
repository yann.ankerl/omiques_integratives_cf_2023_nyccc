#!/usr/bin/bash

accessions=(SRR2960405 SRR2960406 SRR2960407 SRR2960417 SRR2960418 SRR2960419 SRR2960423 SRR2960424 SRR2960425 SRR2960435 SRR2960436 SRR2960437)
DATA_DIR=/home/projet/SRR_files

# 1. FastQC pre-trimming
#OUTDIR_FPRE=/home/projet/qcPreTrimming
OUTDIR_FPRE=/home/naell/test1212/qcPreTrimming

echo "Running FastQC on raw data..."

for acc in "${accessions[@]}"; do
        nohup fastqc $DATA_DIR/"$acc"_1.fastq -o $OUTDIR_FPRE &
	nohup fastqc $DATA_DIR/"$acc"_2.fastq -o $OUTDIR_FPRE &
done
wait


# 2. MultiQC pre-trimming
#OUTDIR_MPRE=/home/projet/multiqcPreT
OUTDIR_MPRE=/home/naell/test1212/MultiqcPreTrimming

echo "Running MultiQC on raw data..."

for acc in "${accessions[@]}"; do
	nohup multiqc -o $OUTDIR_MPRE $OUTDIR_FPRE/"$acc"_1_fastqc.zip $OUTDIR_FPRE/"$acc"_2_fastqc.zip -n multipre_report_"$acc" &
done
wait


# 3. TrimGalore
#OUTDIR_TRIM=/home/projet/trimmed_data
OUTDIR_TRIM=/home/naell/test1212/trimmed_data

echo "Running TrimGalore..."

for acc in "${accessions[@]}"; do
        nohup trim_galore --rrbs --paired -q 30 --clip_R1 5 --clip_R2 5 --three_prime_clip_R1 3 --three_prime_clip_R2 3  $DATA_DIR/"$acc"_1.fastq  $DATA_DIR/"$acc"_2.fastq -o $OUTDIR_TRIM &
done
wait


# 4. FastQC post-trimming
#OUTDIR_FPOST=/home/projet/qcPostTrimming
OUTDIR_FPOST=/home/naell/test1212/qcPostTrimming

echo "Running FastQC on trimmed data..."

for acc in "${accessions[@]}"; do
        nohup fastqc $OUTDIR_TRIM/"$acc"_1_val_1.fq -o $OUTDIR_FPOST &
	nohup fastqc $OUTDIR_TRIM/"$acc"_2_val_2.fq -o $OUTDIR_FPOST &
done
wait


# 5. MultiQC post-trimming
#OUTDIR_MPOST=/home/projet/multiqcPostT
OUTDIR_MPOST=/home/naell/test1212/MultiqcPostTrimming

echo "Running MultiQC on trimmed data..."

for acc in "${accessions[@]}"; do
	nohup multiqc -o $OUTDIR_MPOST $OUTDIR_FPOST/"$acc"_1_val_1_fastqc.zip  $OUTDIR_FPOST/"$acc"_2_val_2_fastqc.zip -n multipost_report_"$acc" &
done
wait


# 6. Alignment (Bismark)
# 6.1. Genome preparation
GENREF_DIR=/home/projet/GenomeRef_mm39/ncbi_dataset/data/GCF_000001635.27

echo "Preparing the reference genome..."

#nohup bismark_genome_preparation $GENREF_DIR &
#wait

# à rajouter : une erreur si le fichier est en .fna et pas en .fasta par exemple

# 6.2. Alignment
#OUTDIR_ALIGN=/home/projet/BismarkResults
OUTDIR_ALIGN=/home/naell/test1212/BismarkResults

echo "Aligning with Bismark..."

for acc in "${accessions[@]}"; do
	nohup bismark $GENREF_DIR -1 $OUTDIR_TRIM/"$acc"_1_val_1.fq -2 $OUTDIR_TRIM/"$acc"_2_val_2.fq -o $OUTDIR_ALIGN &
done
wait

# 7. Methylation analysis
# 7.1. BAM sorting
#OUTDIR_SORT=/home/projet/BismarkResults/sorted
OUTDIR_SORT=/home/naell/test1212/BismarkResults/sorted

mkdir -p $OUTDIR_SORT
cd $OUTDIR_SORT

echo "Sorting bam files..."

for file in $OUTDIR_ALIGN/*.bam; do
	sorted_file=$(basename $file .bam)_sorted.bam
	samtools sort $file > $sorted_file &
done
wait

# 7.2. MethylKit
echo "Performing methylation analysis with methylKit, R..."
SCRIPTS=/omiques_integratives_cf_2023_nyccc/scripts

OUTDIR_SORT=/home/naell/methkit/BismarkResults/sorted
control_0h=($(ls $OUTDIR_SORT/SRR296040* | xargs -n 1 basename))
control_18h=($(ls $OUTDIR_SORT/SRR296041* | xargs -n 1 basename))
ikaros_0h=($(ls $OUTDIR_SORT/SRR296042* | xargs -n 1 basename))
ikaros_18h=($(ls $OUTDIR_SORT/SRR296043* | xargs -n 1 basename))

cd $OUTDIR_SORT

echo "Running methylkit : control..."
nohup Rscript /home/projet/scripts/mini_scripts/Mkit_control_100.R ${control_0h[0]} ${control_0h[1]} ${control_0h[2]} ${control_18h[0]} ${control_18h[1]} ${control_18h[2]} &
nohup Rscript /home/projet/scripts/mini_scripts/Mkit_control_500.R ${control_0h[0]} ${control_0h[1]} ${control_0h[2]} ${control_18h[0]} ${control_18h[1]} ${control_18h[2]} &
nohup Rscript /home/projet/scripts/mini_scripts/Mkit_control_1000.R ${control_0h[0]} ${control_0h[1]} ${control_0h[2]} ${control_18h[0]} ${control_18h[1]} ${control_18h[2]} &

echo "Running methylkit : ikaros..."
nohup Rscript /home/projet/scripts/mini_scripts/Mkit_ikaros_100.R ${ikaros_0h[0]} ${ikaros_0h[1]} ${ikaros_0h[2]} ${ikaros_18h[0]} ${ikaros_18h[1]} ${ikaros_18h[2]} &
nohup Rscript /home/projet/scripts/mini_scripts/Mkit_ikaros_500.R ${ikaros_0h[0]} ${ikaros_0h[1]} ${ikaros_0h[2]} ${ikaros_18h[0]} ${ikaros_18h[1]} ${ikaros_18h[2]} &
nohup Rscript /home/projet/scripts/mini_scripts/Mkit_ikaros_1000.R ${ikaros_0h[0]} ${ikaros_0h[1]} ${ikaros_0h[2]} ${ikaros_18h[0]} ${ikaros_18h[1]} ${ikaros_18h[2]} &

echo "Running methylkit : T0..."
nohup Rscript /home/projet/scripts/mini_scripts/Mkit_0h_100.R ${control_0h[0]} ${control_0h[1]} ${control_0h[2]} ${ikaros_0h[0]} ${ikaros_0h[1]} ${ikaros_0h[2]} &
nohup Rscript /home/projet/scripts/mini_scripts/Mkit_0h_500.R ${control_0h[0]} ${control_0h[1]} ${control_0h[2]} ${ikaros_0h[0]} ${ikaros_0h[1]} ${ikaros_0h[2]} &
nohup Rscript /home/projet/scripts/mini_scripts/Mkit_0h_1000.R ${control_0h[0]} ${control_0h[1]} ${control_0h[2]} ${ikaros_0h[0]} ${ikaros_0h[1]} ${ikaros_0h[2]} &

echo "Running methylkit : T18..."
nohup Rscript /home/projet/scripts/mini_scripts/Mkit_18h_100.R ${control_18h[0]} ${control_18h[1]} ${control_18h[2]} ${ikaros_18h[0]} ${ikaros_18h[1]} ${ikaros_18h[2]} &
nohup Rscript /home/projet/scripts/mini_scripts/Mkit_18h_500.R ${control_18h[0]} ${control_18h[1]} ${control_18h[2]} ${ikaros_18h[0]} ${ikaros_18h[1]} ${ikaros_18h[2]} &
nohup Rscript /home/projet/scripts/mini_scripts/Mkit_18h_1000.R ${control_18h[0]} ${control_18h[1]} ${control_18h[2]} ${ikaros_18h[0]} ${ikaros_18h[1]} ${ikaros_18h[2]} &

wait

mv *control_100.bedGraph methylKit_control_100/
mv *control_500.bedGraph methylKit_control_500/
mv *control_1000.bedGraph methylKit_control_1000/

mv *ikaros_100.bedGraph methylKit_ikaros_100/
mv *ikaros_500.bedGraph methylKit_ikaros_500/
mv *ikaros_1000.bedGraph methylKit_ikaros_1000/

mv *0h_100.bedGraph methylKit_0h_100/
mv *0h_500.bedGraph methylKit_0h_500/
mv *0h_1000.bedGraph methylKit_0h_1000/

mv *18h_100.bedGraph methylKit_18h_100/
mv *18h_500.bedGraph methylKit_18h_500/
mv *18h_1000.bedGraph methylKit_18h_1000/

echo "Methylkit analysis done for all conditions."


# 8. featureCounts
# Mapped genes
ANNOTGFF=/home/projet/GenomeRef_mm39/ncbi_dataset/data/GCF_000001635.27/genomic.gff

awk -F "\t" 'BEGIN{OFS="\t"} $3=="gene" {print $0}' $ANNOTGFF > genomic_genes.gff

nohup featureCounts --countReadPairs -p -t gene -g ID -a genomic_genes.gff -o counts_0h.txt ${control_0h[@]} ${ikaros_0h[@]} &
nohup featureCounts --countReadPairs -p -t gene -g ID -a genomic_genes.gff -o counts_18h.txt ${control_18h[@]} ${ikaros_18h[@]} &
nohup featureCounts --countReadPairs -p -t gene -g ID -a genomic_genes.gff -o counts_control.txt ${control_0h[@]} ${control_18h[@]} &
nohup featureCounts --countReadPairs -p -t gene -g ID -a genomic_genes.gff -o counts_ikaros.txt ${ikaros_0h[@]} ${ikaros_18h[@]} &
wait

for counts in $OUTDIR_SORT/counts_*.txt ; do
        awk 'BEGIN{OFS="\t"} $7>10 && $8>10 && $9>10 && $10>10 {print $0}' $counts > "$(basename $counts .txt)"_10cov.txt
        rm $counts
done
wait

cat *_10cov.txt | grep 'gene-' | awk '{print $1}' | cut -c 6- | sort -u > rrbs_mapped_genes.txt

# 9. bedtools merge / bedtools window
# Merge all DMRs
bedtools merge methylKit_18h_100/hyper* methylKit_18h_500/hyper* methylKit_18h_1000/hyper* > hyperDMR_18h_merged.bedGraph
bedtools merge methylKit_0h_100/hyper* methylKit_0h_500/hyper* methylKit_0h_1000/hyper* > hyperDMR_0h_merged.bedGraph
bedtools merge methylKit_control_100/hyper* methylKit_control_500/hyper* methylKit_control_1000/hyper* > hyperDMR_control_merged.bedGraph

bedtools merge methylKit_18h_100/hypo* methylKit_18h_500/hypo* methylKit_18h_1000/hypo* > hypoDMR_18h_merged.bedGraph
bedtools merge methylKit_0h_100/hypo* methylKit_0h_500/hypo* methylKit_0h_1000/hypo* > hypoDMR_0h_merged.bedGraph
bedtools merge methylKit_control_100/hypo* methylKit_control_500/hypo* methylKit_control_1000/hypo* > hypoDMR_control_merged.bedGraph

# DMR-associated genes
bedtools window -a genomic_genes.gff -b hyperDMR_18h_merged.bedGraph > genes18hype.gff
bedtools window -a genomic_genes.gff -b hypoDMR_18h_merged.bedGraph > genes18hypo.gff

bedtools window -a genomic_genes.gff -b hyperDMR_0h_merged.bedGraph > genes0hype.gff
bedtools window -a genomic_genes.gff -b hypoDMR_0h_merged.bedGraph > genes0hypo.gff

bedtools window -a genomic_genes.gff -b hyperDMR_control_merged.bedGraph > geneschype.gff
bedtools window -a genomic_genes.gff -b hypoDMR_control_merged.bedGraph > geneschypo.gff

awk -F "[=;]" 'BEGIN {OFS="\t"} {print $2}' genes0hype.gff | cut -c 6- > list_genes0hype.txt
awk -F "[=;]" 'BEGIN {OFS="\t"} {print $2}' genes18hype.gff | cut -c 6- > list_genes18hype.txt
awk -F "[=;]" 'BEGIN {OFS="\t"} {print $2}' geneschype.gff | cut -c 6- > list_geneschype.txt

awk -F "[=;]" 'BEGIN {OFS="\t"} {print $2}' genes0hypo.gff | cut -c 6- > list_genes0hypo.txt
awk -F "[=;]" 'BEGIN {OFS="\t"} {print $2}' genes18hypo.gff | cut -c 6- > list_genes18hypo.txt
awk -F "[=;]" 'BEGIN {OFS="\t"} {print $2}' geneschypo.gff | cut -c 6- > list_geneschypo.txt

# List DMR genes
grep -vf <(cat list_geneschype.txt) <(grep -vf <(cat list_genes0hype.txt) <(cat list_genes18hype.txt)) > DMR_hyper_genes.txt
grep -vf <(cat list_geneschypo.txt) <(grep -vf <(cat list_genes0hypo.txt) <(cat list_genes18hypo.txt)) > DMR_hypo_genes.txt


