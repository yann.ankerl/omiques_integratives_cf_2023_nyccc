#!/usr/bin/bash

accessions=(SRR2960405 SRR2960406 SRR2960407 SRR2960417 SRR2960418 SRR2960419 SRR2960423 SRR2960424 SRR2960425 SRR2960435 SRR2960436 SRR2960437) 

for acc in "${accessions[@]}"; do
        nohup trim_galore --rrbs --paired -q 30 --clip_R1 5 --clip_R2 5 --three_prime_clip_R1 3 --three_prime_clip_R2 3 "$acc"_1.fastq "$acc"_2.fastq -o /home/projet/Trimming &
done

